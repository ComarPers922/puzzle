/** Author: Yuhang Liao (b9007287)
* Created on: October 11th, 2019
*/

#include "PuzzleBoard.h"

int PuzzleBoard::get_number_of_continuous_groups(std::vector<int>& vec, int size_group)
{
	std::sort(vec.begin(), vec.end());
	int num_groups = 0;
	for (size_t i = 0; i + size_group - 1 < vec.size(); i++)
	{
		int previous_num = vec[i];
		bool is_valid = true;
		for (size_t j = 1; j < size_group; j++)
		{
			if (vec[i + j] - previous_num != 1)
			{
				is_valid = false;
				break;
			}
			previous_num = vec[i + j];
		}
		if (is_valid)
		{
			num_groups++;
		}
	}
	return num_groups;
}

PuzzleBoard::PuzzleBoard(const int size) :
	board_size(size),
	board(new int* [size]),
	number_count(0)
{
	if (size <= 0)
	{
		return;
	}
	for (int i = 0; i < size; i++)
	{
		board[i] = new int[size];
	}
	board[size - 1][size - 1] = SPACE;
}

PuzzleBoard::PuzzleBoard(const PuzzleBoard& board):
	board_size(board.board_size),
	board(new int* [board.board_size]),
	number_count(0)
{
	for (int i = 0; i < board_size; i++)
	{
		this->board[i] = new int[board_size];
	}
	for (int i = 0; i < board_size; i++)
	{
		for (int j = 0; j < board_size; j++)
		{
			//number_set.insert(board.board[i][j]);
			//this->board[i][j] = board.board[i][j];
			//number_count++;
			this->add_new_number(board.board[i][j]);
		}
	}
	this->board[board_size - 1][board_size - 1] = SPACE;
}

PuzzleBoard::~PuzzleBoard()
{
	for (int i = 0; i < board_size; i++)
	{
		delete[] board[i];
	}
	delete[] board;
}

int PuzzleBoard::get_board_size() const
{
	return board_size;
}

int PuzzleBoard::get_number_count() const
{
	return number_count;
}

bool PuzzleBoard::add_new_number(const int new_num)
{
	if (number_set.find(new_num) != number_set.end() ||
		is_board_full() || 
		new_num <= 0)
	{
		return false;
	}
	number_set.insert(new_num);
	board[number_count / board_size][number_count % board_size] = new_num;
	number_count++;
}

bool PuzzleBoard::is_board_full() const
{
	return number_count >= board_size * board_size - 1;
}

bool PuzzleBoard::is_valid_turn() const
{
	return is_board_full() && board[board_size - 1][board_size - 1] == SPACE;
}

int PuzzleBoard::get_num_of_valid_rows() const
{
	if (!is_valid_turn())
	{
		return -1;
	}
	int count = 0;
	for (int i = 0; i < board_size; i++)
	{
		bool isValid = true;
		int previous_value = board[i][0];
		for (int j = 1; j < board_size; j++)
		{
			if (j == board_size - 1 && 
				i == board_size - 1 &&
				board[i][j] == SPACE)
			{
				break;
			}
			if (board[i][j] - previous_value != 1)
			{
				isValid = false;
			}
			previous_value = board[i][j];
		}
		if (isValid)
		{
			count++;
		}
	}
	return count;
}

int PuzzleBoard::get_num_of_valid_columns() const
{
	if (!is_valid_turn())
	{
		return -1;
	}
	int count = 0;
	for (int i = 0; i < board_size; i++)
	{
		int previous_value = board[0][i];
		bool isValid = true;
		for (int j = 1; j < board_size; j++)
		{
			if (j == board_size - 1 &&
				i == board_size - 1 &&
				board[i][j] == SPACE)
			{
				break;
			}
			if (board[j][i] - previous_value != 1)
			{
				isValid = false;
				break;
			}
			previous_value = board[j][i];
		}
		if (isValid)
		{
			count++;
		}
	}
	return count;
}

int PuzzleBoard::get_num_of_valid_rows_reversed() const
{
	if (!is_valid_turn())
	{
		return -1;
	}
	int count = 0;
	for (int i = board_size - 1; i >= 0; i--)
	{
		bool isValid = true;
		int previous_value = board[i][board_size - 1];
		for (int j = board_size - 2; j >= 0; j--)
		{
			if (j == board_size - 1 &&
				i == board_size - 1 &&
				board[i][j] == SPACE)
			{
				continue;
			}
			if (previous_value != SPACE && board[i][j] - previous_value != 1)
			{
				isValid = false;
			}
			previous_value = board[i][j];
		}
		if (isValid)
		{
			count++;
		}
	}
	return count;
}

int PuzzleBoard::get_num_of_valid_columns_reversed() const
{
	if (!is_valid_turn())
	{
		return -1;
	}
	int count = 0;
	for (int i = board_size - 1; i >= 0; i--)
	{
		int previous_value = board[board_size - 1][i];
		bool isValid = true;
		for (int j = board_size - 2; j >= 0; j--)
		{
			if (j == board_size - 1 &&
				i == board_size - 1 &&
				board[i][j] == SPACE)
			{
				continue;
			}
			if (previous_value != SPACE && board[j][i] - previous_value != 1)
			{
				isValid = false;
				break;
			}
			previous_value = board[j][i];
		}
		if (isValid)
		{
			count++;
		}
	}
	return count;
}

std::string PuzzleBoard::get_hash() const
{
	std::stringstream result;
	for (int i = 0; i < board_size; i++)
	{
		for (int j = 0; j < board_size; j++)
		{
			result << std::to_string(board[i][j]);
		}
	}
	return result.str();
}

PuzzleBoard PuzzleBoard::randomly_generate(const int size, const int max) throw (std::invalid_argument)
{
	if (max < size * size - 1)
	{
		throw std::invalid_argument("Not enough numbers for the board.");
	}
	PuzzleBoard result(size);
	std::vector<std::deque<int>> bucket;
	int bucket_size = (size > 50 ? 50 : size);
	for (int i = 0; i < bucket_size; i++)
	{
		bucket.push_back(std::deque<int>());
	}

	for (int i = 0; i < max; i++)
	{
		if (Utils::get_random_integer() % 100 > 50)
		{
			bucket[Utils::get_random_integer() % bucket_size].push_back(i);
		}
		else
		{
			bucket[Utils::get_random_integer() % bucket_size].push_front(i);
		}
	}

	int count = 0, index = 0;
	while (!result.is_board_full())
	{
		if (bucket[index].size() > 0)
		{
			while (!bucket[index].empty())
			{
				int rand_num = Utils::get_random_integer() % 100;
				if (rand_num > 50)
				{
					result.add_new_number(bucket[index].front());
					bucket[index].pop_front();
				}
				else
				{
					result.add_new_number(bucket[index].back());
					bucket[index].pop_back();
				}
				if (result.is_board_full())
				{
					break;
				}
			}
		}
		index++;
	}
	result.board[result.board_size - 1][result.board_size - 1] = SPACE;
	return result;
}

std::ostream& operator << (std::ostream& ostr, const PuzzleBoard& board)
{
	for (int i = 0; i < board.board_size; i++)
	{
		for (int j = 0; j < board.board_size; j++)
		{
			if (board.board[i][j] > 0)
			{
				ostr << board.board[i][j] << '\t';
			}
			else
			{
				ostr << " \t" << std::endl;
			}
		}
		ostr << std::endl;
	}
	return ostr;
}

std::ostream& operator << (std::ostream& ostr, const PuzzleBoard* const board)
{
	for (int i = 0; i < board->board_size; i++)
	{
		for (int j = 0; j < board->board_size; j++)
		{
			if (board->board[i][j] > 0)
			{
				ostr << board->board[i][j] << '\t';
			}
			else
			{
				ostr << " \t" << std::endl;
			}
		}
		ostr << std::endl;
	}
	return ostr;
}

PuzzleBoard& PuzzleBoard::operator=(const PuzzleBoard& puzzleboard)
{
	for (int i = 0; i < this->board_size; i++)
	{
		delete[] this->board[i];
	}
	delete[] this->board;

	this->board_size = puzzleboard.board_size;
	this->number_count = 0;
	this->board = new int* [puzzleboard.board_size];
	for (int i = 0; i < puzzleboard.board_size; i++)
	{
		this->board[i] = new int[puzzleboard.board_size];
	}
	this->number_set.clear();
	for (int i = 0; i < this->board_size; i++)
	{
		for (int j = 0; j < this->board_size; j++)
		{
			this->board[i][j] = puzzleboard.board[i][j];
			this->number_set.insert(puzzleboard.board[i][j]);
			number_count++;
		}
	}
	return *this;
}

PuzzleBoard::BoardResult PuzzleBoard::solve_whole_board()
{
	BoardResult result;
	if (!this->is_board_full())
	{
		return result;
	}
	std::vector<int> vec;
	for (int i = 0; i < board_size; i++)
	{
		for (int j = 0; j < board_size; j++)
		{
			vec.push_back(board[i][j]);
		}
	}
	auto num_groups_size = BigUnsignedInteger(get_number_of_continuous_groups(vec, board_size));
	auto num_groups_size_m_1 = BigUnsignedInteger(get_number_of_continuous_groups(vec, board_size - 1));
	auto fact_n1 = BigUnsignedInteger::factorial_devided_by_2(BigUnsignedInteger((unsigned long long)board_size * board_size - board_size - 1));
	auto fact = BigUnsignedInteger::factorial_devided_by_2(BigUnsignedInteger((unsigned long long)board_size * board_size - board_size));
	auto result_num = num_groups_size_m_1 * fact + num_groups_size * (fact_n1 * ((unsigned long long)board_size - 1));
	result.column = result.row = result.reversed_column = result.reversed_row = result_num;
	return result;
}

PuzzleBoard::BoardResult PuzzleBoard::solve_partially(int num) throw(std::invalid_argument)
{
	if (num < 2 || num > board_size)
	{
		throw std::invalid_argument("The parameter is illegal!!!");
	}

	BoardResult result;
	if (!this->is_board_full())
	{
		return result;
	}
	std::vector<int> vec;
	for (int i = 0; i < board_size; i++)
	{
		for (int j = 0; j < board_size; j++)
		{
			vec.push_back(board[i][j]);
		}
	}
	auto num_groups_size = BigUnsignedInteger(get_number_of_continuous_groups(vec, num));
	auto num_groups_size_m_1 = BigUnsignedInteger(get_number_of_continuous_groups(vec, num - 1));
	auto fact_n1 = BigUnsignedInteger::factorial_devided_by_2(BigUnsignedInteger((unsigned long long)board_size * board_size - num - 1));
	auto fact = BigUnsignedInteger::factorial_devided_by_2(BigUnsignedInteger((unsigned long long)board_size * board_size - num));
	auto result_num = num_groups_size_m_1 * fact + num_groups_size * (fact_n1 * ((unsigned long long)board_size - 1));
	result.column = result.row = result.reversed_column = result.reversed_row = result_num;
	return result;
}

PuzzleBoard::BoardResult PuzzleBoard::solve_partially(int num, PuzzleBoard::BoardResult& partial_result)
{
	unsigned long long row = 0, column = 0, row_reversed = 0, column_reversed = 0;
	for (int i = 0; i < board_size; i++)
	{
		for (int j = 0; j + num <= board_size; j++)
		{
			int previous = board[i][j];
			bool is_valid = true;
			for (int k = 1; k < num; k++)
			{
				if (board[i][k + j] - previous != 1 && board[i][k + j] != SPACE)
				{
					is_valid = false;
					break;
				}
				previous = board[i][k + j];
			}
			if (is_valid)
			{
				row++;
			}
		}
	}

	for (int i = 0; i < board_size; i++)
	{
		for (int j = 0; j + num <= board_size; j++)
		{
			int previous = board[j][i];
			bool is_valid = true;
			for (int k = 1; k < num; k++)
			{
				if (board[k + j][i] - previous != 1 && board[i][k + j] != SPACE)
				{
					is_valid = false;
					break;
				}
				previous = board[k + j][i];
			}
			if (is_valid)
			{
				column++;
			}
		}
	}

	for (int i = 0; i < board_size; i++)
	{
		for (int j = 0; j + num <= board_size; j++)
		{
			int previous = board[i][j];
			bool is_valid = true;
			for (int k = 1; k < num; k++)
			{
				if (board[i][k + j] - previous != -1 && board[i][k + j] != SPACE)
				{
					is_valid = false;
					break;
				}
				previous = board[i][k + j];
			}
			if (is_valid)
			{
				row_reversed++;
			}
		}
	}

	for (int i = 0; i < board_size; i++)
	{
		for (int j = 0; j + num <= board_size; j++)
		{
			int previous = board[j][i];
			bool is_valid = true;
			for (int k = 1; k < num; k++)
			{
				if (board[k + j][i] - previous != -1 && board[i][k + j] != SPACE)
				{
					is_valid = false;
					break;
				}
				previous = board[k + j][i];
			}
			if (is_valid)
			{
				column_reversed++;
			}
		}
	}
	partial_result.row = row;
	partial_result.column = column;
	partial_result.reversed_row = row_reversed;
	partial_result.reversed_column = column_reversed;
	return solve_partially(num);
}
