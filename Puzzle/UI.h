/** Author: Yuhang Liao (b9007287)
* Created on: October 17th, 2019
*/

#pragma once
#include <string>
#include <functional>

using std::string;
using std::function;

class UI
{
public:
	UI(const string description = "", const function<void()> action = []() {});
	UI(const UI& ui);
	UI& operator=(const UI& ui);
	string get_description() const;
	function<void()> get_action() const;
private:
	string description;
	function<void()> action;
};