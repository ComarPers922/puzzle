/** Author: Yuhang Liao (b9007287)
* Created on: October 18th, 2019
*/

#include "FileManager.h"

FileManager::FileManager(const string input_loc, const string output_loc) :
	input_path(input_loc),
	output_path(output_loc)
{
}

FileManager::~FileManager()
{
	input_stream.close();
	output_stream.close();
}

string FileManager::get_input_path() const
{
	return input_path;
}

string FileManager::get_output_path() const
{
	return output_path;
}

void FileManager::set_input_path(const string path)
{
	input_path = path;
}

void FileManager::set_output_path(const string path)
{
	output_path = path;
}

string FileManager::get_output_buffer() const
{
	return output_buffer;
}

string FileManager::get_input_buffer() const
{
	return input_buffer;
}

void FileManager::set_input_buffer(const string content)
{
	input_buffer = content;
}

void FileManager::read_file()
{
	input_buffer = "";
	stringstream result;
	input_stream.open(input_path);
	if (!input_stream.is_open())
	{
		input_stream.close();
		return;
	}
	result << input_stream.rdbuf();
	input_buffer = result.str();
	input_stream.close();
}

void FileManager::write_file()
{
	output_stream.open(output_path);
	if (!output_stream.is_open())
	{
		output_stream.close();
		return;
	}
	output_stream << output_buffer;
	output_stream.close();
}
