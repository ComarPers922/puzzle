/** Author: Yuhang Liao (b9007287)
* Created on: October 22th, 2019
*/

#include "BigUnsignedInteger.h"

BigUnsignedInteger::BigUnsignedInteger(const char* str)
{
	int index = 0;
	while (str[index] != '\0')
	{
		number.push_back(str[index] - '0');
		index++;
	}
	if (number.empty())
	{
		number.push_back(0);
	}
}

BigUnsignedInteger::BigUnsignedInteger(const unsigned long long num)
{
	auto temp = num;
	while(temp)
	{ 
		number.push_back(temp % 10);
		temp /= 10;
	}
	if (number.empty())
	{
		number.push_back(0);
	}
}

BigUnsignedInteger::BigUnsignedInteger(const BigUnsignedInteger& other)
{
	number.clear();
	for (const auto& item : other.number)
	{
		number.push_back(item);
	}
}

BigUnsignedInteger::BigUnsignedInteger()
{
	number.push_back(0);
}

BigUnsignedInteger BigUnsignedInteger::add(const BigUnsignedInteger& other)
{
	char carry = 0;
	auto& other_num = other.number;
	deque<short> this_num(this->number);
	int index = 0;
	for (; index < other_num.size() && index < this_num.size(); index++)
	{
		int temp = other_num[index] + this_num[index] + carry;
		carry = temp / 10;
		temp %= 10;
		this_num[index] = temp;
	}
	for (; index < other_num.size(); index++)
	{
		int temp = other_num[index] + carry;
		carry = temp / 10;
		temp %= 10;
		this_num.push_back(temp);
	}
	while (carry > 0)
	{
		if (index < this_num.size())
		{
			int temp = this_num[index] + carry;
			carry = temp / 10;
			temp %= 10;
			this_num[index] = temp;
		}
		else
		{
			this_num.push_back(carry);
			carry = 0;
		}
		index++;
	}
	return BigUnsignedInteger(this_num);
}

BigUnsignedInteger BigUnsignedInteger::mul(const BigUnsignedInteger& other)
{
	BigUnsignedInteger result((unsigned long long)0);
	int num_zeros = 0;
	for (const auto& item : other.number)
	{
		auto temp = this->multiple_with_single_digit(item);
		for (int i = 0; i < num_zeros; i++)
		{
			temp.number.push_front(0);
		}
		result = result + temp;
		num_zeros++;
	}
	return result;
}

unsigned long long BigUnsignedInteger::toULL() const
{
	unsigned long long result = 0;
	for (auto item = number.rbegin(); item != number.rend(); item++)
	{
		result *= 10;
		result += *item;
	}
	return result;
}

BigUnsignedInteger BigUnsignedInteger::operator+(const BigUnsignedInteger& other)
{
	return this->add(other);
}

BigUnsignedInteger BigUnsignedInteger::operator+(const unsigned long long& other)
{
	return this->add(BigUnsignedInteger(other));
}

BigUnsignedInteger BigUnsignedInteger::operator*(const BigUnsignedInteger& other)
{
	return this->mul(other);
}

BigUnsignedInteger BigUnsignedInteger::operator*(const unsigned long long& other)
{
	return this->mul(BigUnsignedInteger(other));
}

BigUnsignedInteger& BigUnsignedInteger::operator=(const BigUnsignedInteger& other)
{
	this->number.clear();
	for (const auto& item : other.number)
	{
		this->number.push_back(item);
	}
	if (this->number.empty())
	{
		this->number.push_back(0);
	}
	return *this;
}

BigUnsignedInteger& BigUnsignedInteger::operator=(const unsigned long long& other)
{
	number.clear();
	unsigned long long temp = other;
	while (temp)
	{
		number.push_back(temp % 10);
		temp /= 10;
	}
	if (this->number.empty())
	{
		this->number.push_back(0);
	}
	return *this;
}

BigUnsignedInteger& BigUnsignedInteger::operator=(const int& other)
{
	number.clear();
	int temp = other;
	while (temp)
	{
		number.push_back(temp % 10);
		temp /= 10;
	}
	if (this->number.empty())
	{
		this->number.push_back(0);
	}
	return *this;
}

bool BigUnsignedInteger::operator==(const BigUnsignedInteger& other)
{
	if (other.number.size() != this->number.size())
	{
		return false;
	}
	for (int i = 0; i < other.number.size(); i++)
	{
		if (other.number[i] != this->number[i])
		{
			return false;
		}
	}
	return true;
}

bool BigUnsignedInteger::operator!=(const BigUnsignedInteger& other)
{
	return !(*this == other);
}

BigUnsignedInteger BigUnsignedInteger::factorial(BigUnsignedInteger num)
{
	const BigUnsignedInteger ONE(1);
	const BigUnsignedInteger ZERO(0);
	if(num == ONE || num == ZERO)
    {
        return BigUnsignedInteger(1);
    }
	BigUnsignedInteger result(1);
	BigUnsignedInteger index(2);
	while (index != num)
	{
		result = result * index;
		index = index + ONE;
	}
	result = result * index;
	return result;
}

BigUnsignedInteger BigUnsignedInteger::factorial_devided_by_2(BigUnsignedInteger num)
{
	const BigUnsignedInteger ONE(1);
	const BigUnsignedInteger ZERO(0);
	if(num == ONE || num == ZERO)
    {
        return BigUnsignedInteger(0);
    }

	BigUnsignedInteger result(1);
	BigUnsignedInteger index(3);
	while (index != num)
	{
		result = result * index;
		index = index + ONE;
	}
	result = result * index;
	return result;
}

BigUnsignedInteger::BigUnsignedInteger(const deque<short>& num)
{
	for (const auto& item : num)
	{
		this->number.push_back(item);
	}
	if (num.size() == 0)
	{
		this->number.push_back(0);
	}
}

BigUnsignedInteger BigUnsignedInteger::multiple_with_single_digit(const short& num)
{
	int carry = 0, index = 0;
	deque<short> this_num(this->number);
	do
	{
		int temp = 0;
		temp = this_num[index] * num + carry;
		carry = temp / 10;
		temp %= 10;
		this_num[index] = temp;
		index++;
	} while (index < this_num.size());
	if (carry > 0)
	{
		this_num.push_back(carry);
	}
	return BigUnsignedInteger(this_num);
}

std::ostream& operator << (std::ostream& ostr, const BigUnsignedInteger& num)
{
	for (auto item = num.number.rbegin(); item != num.number.rend(); item ++)
	{
		ostr << *item;
	}
	return ostr;
}

BigUnsignedInteger& BigUnsignedInteger::operator*= (const BigUnsignedInteger& other)
{
    *this = this->mul(other);
    return *this;
}

BigUnsignedInteger& BigUnsignedInteger::operator*= (const unsigned long long& other)
{
    *this = this->mul(other);
    return *this;
}
