/** Author: Yuhang Liao (b9007287)
* Created on: October 11th, 2019
*/

#include <iostream>
#include <string>
#include <cstdlib>
#include <unordered_set>
#include <thread>
#include <queue>
#include <vector>

#include "PuzzleBoard.h"
#include "Utils.h"
#include "UI.h"
#include "FileManager.h"
#include "BoardFile.h"
#include "BigUnsignedInteger.h"

using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::thread;
using std::queue;
using std::vector;
using std::mutex;

constexpr int NUM_UI_ITEMS = 4;
constexpr int NUM_OF_THREADS = 10;

UI uis[NUM_UI_ITEMS];

const char* input_file_path = "t.15file";
const char* output_file_path = "t.15sol";
BoardFile board_file(input_file_path, output_file_path);

mutex queue_mutex;
mutex index_mutex;

void solve_whole_board(int index, vector<PuzzleBoard>* board_vector, PuzzleBoard::BoardResult* results, int len)
{
	PuzzleBoard current_board;
	auto _board_vector = *board_vector;
	{
		std::lock_guard<mutex> locker(queue_mutex);
		if (index >= len)
		{
			return;
		}
		current_board = _board_vector[index];
	}
	results[index] = current_board.solve_whole_board();
}

void thread_func_whole(vector<PuzzleBoard>* board_vector, PuzzleBoard::BoardResult* results, int len)
{
	static int index = 0;
	while (index < len)
	{
		{
			std::lock_guard<mutex> locker(index_mutex);
			solve_whole_board(index++, board_vector, results, len);
		}
	}
}

void solve_whole_partial(int index, vector<PuzzleBoard>* board_vector, PuzzleBoard::BoardResult* results, PuzzleBoard::BoardResult* partial_results, int len, int partial_num)
{
	PuzzleBoard current_board;
	auto _board_vector = *board_vector;
	{
		std::lock_guard<mutex> locker(queue_mutex);
		if (index >= len)
		{
			return;
		}
		current_board = _board_vector[index];
	}
	results[index] = current_board.solve_partially(partial_num, partial_results[index]);
}

void thread_func_partial(vector<PuzzleBoard>* board_vector, PuzzleBoard::BoardResult* results, PuzzleBoard::BoardResult* partial_results, int len, int partial_num)
{
	static int index = 0;
	while (index < len)
	{
		{
			std::lock_guard<mutex> locker(index_mutex);
			solve_whole_partial(index++, board_vector, results, partial_results, len, partial_num);
		}
	}
}

void initialize_uis(int board_size)
{
	ios::sync_with_stdio(false);
	uis[0] = UI("Manually type a puzzle.", 
		[board_size]()
		{
			PuzzleBoard board(board_size);
			while (!board.is_board_full())
			{
				cout << "Current count of numbers: " << board.get_number_count()
					<< ", Please enter a unique number: ";
				string user_input;
				cin >> user_input;
				if (!Utils::is_integer(user_input))
				{
					cout << "Invalid input format, please try another one!" << endl;
					continue;
				}
				int user_num = atoi(user_input.c_str());
				if (user_num <= 0 || user_num > board_size * board_size + 4)
				{
					cout << "The number must in the range of [1,"<< board_size * board_size + 4 <<"], please try another one!" << endl;
					continue;
				}
				if (!board.is_board_full() &&
					!board.add_new_number(user_num))
				{
					cout << "The input number has already been added, please try another one." << endl;
				}
			}
			cout << "The puzzle is: " << endl << board << endl;
			board_file.write_puzzle(board);
			cout << "The puzzle file was successfully written." << endl;

			cout << "The solution is: " << endl << endl;
			auto result = board.solve_whole_board();
			cout << board << endl;
			cout << "row = " << result.row << endl;
			cout << "column = " << result.column << endl;
			cout << "reversed row = " << result.reversed_row << endl;
			cout << "reversed column = " << result.reversed_column << endl;
			cout << endl;

			board_file.write_puzzle_solution(result, board);
			cout << "Solution file was successfully written as: " << output_file_path << endl;
		});

	uis[1] = UI("Generate random puzzles.",
		[board_size]()
		{
			vector<PuzzleBoard> board_vector;
			cout << "Input a number of puzzles to be generated: ";
			string user_input;
			cin >> user_input;
			if (!Utils::is_integer(user_input))
			{
				cout << "The input is not a number!" << endl;
				return;
			}
			int user_num = atoi(user_input.c_str());
			if (user_num <= 0)
			{
				cout << "The input must be larger than zero." << endl;
				return;
			}
			PuzzleBoard* boards = new PuzzleBoard[user_num];
			for (int i = 0; i < user_num; i++)
			{
				boards[i] = PuzzleBoard::randomly_generate(board_size, board_size * board_size + 4);
				board_vector.push_back(boards[i]);
			}
			for (int i = 0; i < user_num; i++)
			{
				cout << "The puzzle " << i << " is: " << endl;
				cout << boards[i] << endl;
			}
			board_file.write_puzzle(user_num, boards);
			cout << "The puzzle file was successfully written." << endl;

			cout << "The solution of random puzzle boards: " << endl << endl;
			PuzzleBoard::BoardResult* board_results =  new PuzzleBoard::BoardResult[user_num];
			
			thread* threads = new thread[NUM_OF_THREADS];
			for (int i = 0; i < NUM_OF_THREADS; i++)
			{
				threads[i] = thread(thread_func_whole, &board_vector, board_results, user_num);
			}
			for (int i = 0; i < NUM_OF_THREADS; i++)
			{
				threads[i].join();
			}

			for (int i = 0; i < user_num; i++)
			{
				cout << boards[i] << endl;
				cout << "row = " << board_results[i].row << endl;
				cout << "column = " << board_results[i].column << endl;
				cout << "reversed row = " << board_results[i].reversed_row << endl;
				cout << "reversed column = " << board_results[i].reversed_column << endl;
				cout << endl;
			}
			board_file.write_puzzle_solution(user_num, board_results, boards);
			cout << "Solution file was successfully written as: " << output_file_path << endl;

			//Clean up
			delete[] threads;
			delete[] board_results;
			delete[] boards;
		});

	uis[2] = UI("Read a puzzle file.",
		[board_size]()
		{
			vector<PuzzleBoard> board_vector;
			int num_boards;
			auto** boards = board_file.read_puzzle_boards(num_boards, board_size);
			for (int i = 0; i < num_boards; i++)
			{
				board_vector.push_back(*boards[i]);
			}
			PuzzleBoard::BoardResult* board_results = new PuzzleBoard::BoardResult[num_boards];
			thread* threads = new thread[NUM_OF_THREADS];
			for (int i = 0; i < NUM_OF_THREADS; i++)
			{
				threads[i] = thread(thread_func_whole, &board_vector, board_results, num_boards);
			}
			for (int i = 0; i < NUM_OF_THREADS; i++)
			{
				threads[i].join();
			}

			for (int i = 0; i < num_boards; i++)
			{
				cout << *boards[i] << endl;
				cout << "row: " << board_results[i].row << endl;
				cout << "column: " << board_results[i].column << endl;
				cout << "reversed row: " << board_results[i].reversed_row << endl;
				cout << "reversed column: " << board_results[i].reversed_column << endl;
				cout << endl;
			}
			board_file.write_puzzle_solution(num_boards, board_results, boards);
			cout << "Solution file was successfully written as: " << output_file_path << endl;
			
			//Clean up
			delete[] threads;
			delete[] board_results;
			for (int i = 0; i < num_boards; i++)
			{
				delete boards[i];
			}
			delete[] boards;
		});

	uis[3] = UI("Read a puzzle file and solve partially.",
		[board_size]() 
		{
			PuzzleBoard::BoardResult* board_results = nullptr;
			PuzzleBoard::BoardResult* partial_board_results = nullptr;
			thread* threads = nullptr;
			PuzzleBoard** boards = nullptr;
			int num_boards = 0;

			vector<PuzzleBoard> board_vector;
			cout << "Please enter a number: " << endl;
			string user_input;
			cin >> user_input;
			if (!Utils::is_integer(user_input))
			{
				cout << "Invalid input format!" << endl;
				return;
			}
			int partial_num = atoi(user_input.c_str());
			try
			{
				boards = board_file.read_puzzle_boards(num_boards, board_size);
				for (int i = 0; i < num_boards; i++)
				{
					board_vector.push_back(*boards[i]);
				}
				board_results = new PuzzleBoard::BoardResult[num_boards];
				partial_board_results = new PuzzleBoard::BoardResult[num_boards];
				threads = new thread[NUM_OF_THREADS];
				for (int i = 0; i < NUM_OF_THREADS; i++)
				{
					threads[i] = thread(thread_func_partial, &board_vector, board_results, partial_board_results, num_boards, partial_num);
				}
				for (int i = 0; i < NUM_OF_THREADS; i++)
				{
					threads[i].join();
				}
				
				for (int i = 0; i < num_boards; i++)
				{
					cout << *boards[i] << endl;
					cout << "Total for row & column, including reverse, in this configuration: " << endl;
					cout << "row = " << partial_board_results[i].row << endl;
					cout << "column = " << partial_board_results[i].column << endl;
					cout << "reversed row = " << partial_board_results[i].reversed_row << endl;
					cout << "reversed column = " << partial_board_results[i].reversed_column << endl;
					
					cout << "Total for row and column, including reverse, for all valid turns: " << endl;
					cout << "row = " << board_results[i].row << endl;
					cout << "column = " << board_results[i].column << endl;
					cout << "reversed row = " << board_results[i].reversed_row << endl;
					cout << "reversed column = " << board_results[i].reversed_column << endl;
					cout << endl;
				}
				//Clean up
				delete[] threads;
				delete[] board_results;
				for (int i = 0; i < num_boards; i++)
				{
					delete boards[i];
				}
				delete[] boards;
				delete[] partial_board_results;
			}
			catch (std::invalid_argument & exception)
			{
				if (threads != nullptr)
				{
					delete[] threads;
				}
				if (board_results != nullptr)
				{
					delete[] board_results;
				}
				if (boards != nullptr)
				{
					for (int i = 0; i < num_boards; i++)
					{
						delete boards[i];
					}
					delete[] boards;
				}
				if (partial_board_results != nullptr)
				{
					delete[] partial_board_results;
				}
				cout << exception.what() << endl;
				return;
			}
		});
}

int main(int argc, char* argv[])
{
	int board_size;
	cout << "Please enter a board size: " << endl;
	cin >> board_size;
	initialize_uis(board_size);
	bool should_program_continue = false;
	do
	{
		should_program_continue = false;

		for (int i = 0; i < NUM_UI_ITEMS; i ++)
		{
			cout << i  << ". "<< uis[i].get_description() << endl;
		}
		cout << endl << "Please make a chioce: ";
		string user_input;
		cin >> user_input;
		if (!Utils::is_integer(user_input))
		{
			cout << "Invalid input format! Please try again..." << endl;
			should_program_continue = true;
			continue;
		}
		int ui_index = atoi(user_input.c_str());
		if (ui_index >= NUM_UI_ITEMS || ui_index < 0)
		{
			cout << "The index number is too big or below zero, please try again..." << endl;
			should_program_continue = true;
			continue;
		}
		cout << endl;
		uis[ui_index].get_action()();
		cout << "Reply Y or y to continue, or exit with other characters" << 
			endl;
		char reply;
		cin >> reply;
		if (reply == 'y' || reply == 'Y')
		{
			should_program_continue = true;
		}
	} while (should_program_continue);
	return 0;
}