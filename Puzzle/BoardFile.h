/** Author: Yuhang Liao (b9007287)
* Created on: October 18th, 2019
*/

#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <exception>

#include "FileManager.h"
#include "PuzzleBoard.h"
#include "Utils.h"

using std::fstream;
using std::ofstream;
using std::ifstream;
using std::ios;
using std::string;
using std::stringstream;
using std::cin;
using std::cout;
using std::endl;

class BoardFile final: public FileManager
{
public:
	BoardFile(const string puzzle_file_location, const string solution_file_location);
	virtual ~BoardFile();

	PuzzleBoard** read_puzzle_boards(int& number_boards,  const int& board_size);
	void write_puzzle_solution(const int& number_boards, PuzzleBoard::BoardResult* const board_results, PuzzleBoard** const boards);
	void write_puzzle_solution(const int& number_boards, PuzzleBoard::BoardResult* const board_results, PuzzleBoard* const boards);
	void write_puzzle_solution(const PuzzleBoard::BoardResult& board_result, const PuzzleBoard& board);
	
	void write_puzzle(const int& number_boards, PuzzleBoard** const boards);
	void write_puzzle(const int& number_boards, PuzzleBoard* const boards);
	void write_puzzle(const PuzzleBoard& board);

private:
	const string puzzle_location;
	const string solution_location;
};
