/** Author: Yuhang Liao (b9007287)
* Created on: October 11th, 2019
*/

#include "Utils.h"

bool Utils::is_integer(const string& text)
{
	for (auto& item : text)
	{
		if (item < '0' ||
			item > '9')
		{
			return false;
		}
	}
	return true;
}

int Utils::get_random_integer()
{
	static int pre = 0;
	srand(time(NULL) + pre);
	pre = rand();
	return pre;
}

unsigned long long Utils::factorial(const unsigned long long num)
{
	unsigned long long result = 1;
	for (size_t i = 2; i <= num; i++)
	{
		result *= i;
	}
	return result;
}
