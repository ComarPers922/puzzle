/** Author: Yuhang Liao (b9007287)
* Created on: October 11th, 2019
*/

#pragma once
#include <string>
#include <cstdlib>
#include <ctime>
#include <thread>
#include <mutex>

#define lock(mut, func) {std::lock_guard <std::mutex> guard (mut); func}

using std::string;

class Utils
{
public:
	static bool is_integer(const string& text);
	static int get_random_integer();
	static unsigned long long factorial(const unsigned long long num);
};

