/** Author: Yuhang Liao (b9007287)
* Created on: October 11th, 2019
*/

#pragma once
#include <iostream>
#include <set>
#include <string>
#include <sstream>
#include <deque>
#include <vector>
#include <cmath>
#include <stdexcept>
#include <algorithm>

#include "Utils.h"
#include "BigUnsignedInteger.h"

constexpr int SPACE = -1;

class PuzzleBoard
{
private:
	std::set<int> number_set;
	int** board;
	int board_size;
	int number_count;
	int get_number_of_continuous_groups(std::vector<int>& vec, int size_group);

public:
	struct BoardResult
	{
		BigUnsignedInteger row;
		BigUnsignedInteger column;
		BigUnsignedInteger reversed_row;
		BigUnsignedInteger reversed_column;
	};
	PuzzleBoard(const int size = 0);
	PuzzleBoard(const PuzzleBoard& board);
	~PuzzleBoard();
	int get_board_size() const;
	int get_number_count() const;
	bool add_new_number(const int new_num);
	bool is_board_full() const;
	bool is_valid_turn() const;
	int get_num_of_valid_rows() const;
	int get_num_of_valid_columns() const;
	int get_num_of_valid_rows_reversed() const;
	int get_num_of_valid_columns_reversed() const;
	std::string get_hash() const;
	friend std::ostream& operator << (std::ostream& ostr, const PuzzleBoard& board);
	friend std::ostream& operator << (std::ostream& ostr, const PuzzleBoard* const board);
	PuzzleBoard& operator=(const PuzzleBoard& puzzleboard);
	BoardResult solve_whole_board();
	BoardResult solve_partially(int num);
	BoardResult solve_partially(int num, BoardResult& partial_result);

	/**
		max is not included. Total size will be size * size
	*/
	static PuzzleBoard randomly_generate(const int size, const int max);
};

