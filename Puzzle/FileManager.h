/** Author: Yuhang Liao (b9007287)
* Created on: October 18th, 2019
*/

#pragma once
#include <string>
#include <fstream>
#include <sstream>

using std::string;
using std::stringstream;
using std::ifstream;
using std::ofstream;

class FileManager
{
public:
	FileManager(const string input_loc, const string output_loc);
	virtual ~FileManager();

	string get_input_path() const;
	string get_output_path() const;

	void set_input_path(const string path);
	void set_output_path(const string path);

	string get_output_buffer() const;
	string get_input_buffer() const;

	void set_input_buffer(const string content);

	void read_file();
	void write_file();
protected:
	string input_path;
	string output_path;
	
	string output_buffer;
	string input_buffer;
private:
	ifstream input_stream;
	ofstream output_stream;
};

