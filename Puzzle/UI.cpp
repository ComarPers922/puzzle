/** Author: Yuhang Liao (b9007287)
* Created on: October 17th, 2019
*/

#include "UI.h"

UI::UI(const string description, const function<void()> action) :
	description(description),
	action(action)
{

}

UI::UI(const UI& ui):
	description(ui.description),
	action(ui.action)
{

}

string UI::get_description() const
{
	return description;
}

function<void()> UI::get_action() const
{
	return action;
}

UI& UI::operator=(const UI& ui)
{
	if (this != &ui)
	{
		this->description = ui.description;
		this->action = ui.action;
	}
	return *this;
}