/** Author: Yuhang Liao (b9007287)
* Created on: October 22th, 2019
*/

#pragma once
#include <iostream>
#include <deque>

#include "Utils.h"

using std::deque;

class BigUnsignedInteger
{
public:
	BigUnsignedInteger(const char* str);
	BigUnsignedInteger(const unsigned long long num);
	BigUnsignedInteger(const BigUnsignedInteger& other);
	BigUnsignedInteger();

	BigUnsignedInteger add(const BigUnsignedInteger& other);
	BigUnsignedInteger mul(const BigUnsignedInteger& other);

	unsigned long long toULL() const;

	BigUnsignedInteger operator+ (const BigUnsignedInteger& other);
	BigUnsignedInteger operator+ (const unsigned long long& other);
	BigUnsignedInteger operator* (const BigUnsignedInteger& other);
	BigUnsignedInteger operator* (const unsigned long long& other);
	BigUnsignedInteger& operator*= (const BigUnsignedInteger& other);
    BigUnsignedInteger& operator*= (const unsigned long long& other);
	BigUnsignedInteger& operator= (const BigUnsignedInteger& other);
	BigUnsignedInteger& operator=(const unsigned long long& other);
	BigUnsignedInteger& operator=(const int& other);
	bool operator==(const BigUnsignedInteger& other);
	bool operator!=(const BigUnsignedInteger& other);

	friend std::ostream& operator << (std::ostream& ostr, const BigUnsignedInteger& num);

	static BigUnsignedInteger factorial(BigUnsignedInteger num);
	static BigUnsignedInteger factorial_devided_by_2(BigUnsignedInteger num);
private:
	BigUnsignedInteger(const deque<short>& num);

	deque<short> number;
	BigUnsignedInteger multiple_with_single_digit(const short& num);
};

