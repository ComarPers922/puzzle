/** Author: Yuhang Liao (b9007287)
* Created on: October 18th, 2019
*/

#include "BoardFile.h"

BoardFile::BoardFile(const string puzzle_file_location, const string solution_file_location) :
	FileManager(puzzle_file_location, solution_file_location),
	puzzle_location(puzzle_file_location),
	solution_location(solution_file_location)
{
}

BoardFile::~BoardFile()
{
}

PuzzleBoard** BoardFile::read_puzzle_boards(int& number_boards, const int& board_size)
{
	read_file();
	if (input_buffer.empty())
	{
		throw std::exception("Unable to open the file.");
		return nullptr;
	}
	int num_boards;
	stringstream stream;
	stream << input_buffer;
	stream >> num_boards;
	number_boards = num_boards;
	PuzzleBoard** result = new PuzzleBoard * [num_boards];
	for (int i = 0; i < num_boards; i++)
	{
		result[i] = new PuzzleBoard(board_size);
		while (!result[i]->is_board_full())
		{
			if (stream.eof())
			{
				return nullptr;
			}
			string file_input;
			stream >> file_input;
			if (!Utils::is_integer(file_input))
			{
				for (int k = 0; k < num_boards; k++)
				{
					delete result[k];
					delete[] result;
					throw std::invalid_argument("File is in invalid format!");
				}
			}
			result[i]->add_new_number(atoi(file_input.c_str()));
		}
	}
	if (!result[num_boards - 1]->is_board_full())
	{
		for (int k = 0; k < num_boards; k++)
		{
			delete result[k];
			delete[] result;
			throw std::invalid_argument("File is in invalid format!");
		}
	}
	return result;
}

void BoardFile::write_puzzle_solution(const int& number_boards, PuzzleBoard::BoardResult* const board_results, PuzzleBoard** const boards)
{
	stringstream stream_buffer;
	stream_buffer << number_boards << endl;
	for (int i = 0; i < number_boards; i++)
	{
		stream_buffer << boards[i] << endl;

		auto* board_result = &board_results[i];
		stream_buffer << "row = " << board_result->row << endl;
		stream_buffer << "column = " << board_result->column << endl;
		stream_buffer << "reversed row = " << board_result->reversed_row << endl;
		stream_buffer << "reversed column = " << board_result->reversed_column << endl;
		stream_buffer << endl;
	}
	output_buffer = stream_buffer.str();
	output_path = solution_location;
	write_file();
}

void BoardFile::write_puzzle_solution(const int& number_boards, PuzzleBoard::BoardResult* const board_results, PuzzleBoard* const boards)
{
	stringstream stream_buffer;
	stream_buffer << number_boards << endl;
	for (int i = 0; i < number_boards; i++)
	{
		stream_buffer << boards[i] << endl;

		auto board_result = board_results[i];
		stream_buffer << "row = " << board_result.row << endl;
		stream_buffer << "column = " << board_result.column << endl;
		stream_buffer << "reversed row = " << board_result.reversed_row << endl;
		stream_buffer << "reversed column = " << board_result.reversed_column << endl;
		stream_buffer << endl;
	}
	output_buffer = stream_buffer.str();
	output_path = solution_location;
	write_file();
}

void BoardFile::write_puzzle_solution(const PuzzleBoard::BoardResult& board_result, const PuzzleBoard& board)
{
	stringstream stream_buffer;
	stream_buffer << 1 << endl;
	stream_buffer << board << endl;

	stream_buffer << "row = " << board_result.row << endl;
	stream_buffer << "column = " << board_result.column << endl;
	stream_buffer << "reversed row = " << board_result.reversed_row << endl;
	stream_buffer << "reversed column = " << board_result.reversed_column << endl;
	stream_buffer << endl;

	output_buffer = stream_buffer.str();
	output_path = solution_location;
	write_file();
}

void BoardFile::write_puzzle(const int& number_boards, PuzzleBoard** const boards)
{
	stringstream stream_buffer;
	stream_buffer << number_boards << endl;

	for (int i = 0; i < number_boards; i++)
	{
		stream_buffer << boards[i] << endl << endl;
	}

	output_buffer = stream_buffer.str();
	output_path = puzzle_location;
	write_file();
}

void BoardFile::write_puzzle(const int& number_boards, PuzzleBoard* const boards)
{
	stringstream stream_buffer;
	stream_buffer << number_boards << endl;

	for (int i = 0; i < number_boards; i++)
	{
		stream_buffer << boards[i] << endl << endl;
	}

	output_buffer = stream_buffer.str();
	output_path = puzzle_location;
	write_file();
}

void BoardFile::write_puzzle(const PuzzleBoard& board)
{
	stringstream stream_buffer;
	stream_buffer << 1 << endl;
	stream_buffer << board << endl << endl;

	output_buffer = stream_buffer.str();
	output_path = puzzle_location;
	write_file();
}
